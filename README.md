# TASK MANAGER

## DEVELOPER INFO

**NAME**: Alexey Filippov

**E-MAIL**: avfilippov@t1-consulting.ru

## SOFTWARE

**OS**: Windows 10

**JDK**: OpenJDK 1.8.0_322

## HARDWARE

**CPU**: i5

**RAM**: 16GB

**SSD**: 512GB

## RUN PROGRAM

``` bash
java -jar ./task_manager.jar
```
